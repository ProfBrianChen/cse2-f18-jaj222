import java.util.Scanner;

public class Pyramid{
  
 public static void main(String args[]){
   
   Scanner numHolder = new Scanner(System.in);
   System.out.println("Enter the side length of the square base of the pyramid: ");
   //This class is so misleading, this should be called SquarePyramid...  Some pyramids can have bases OTHER than square >.> i.e triangular pyramids
   int baseLength = numHolder.nextInt();
   System.out.println("Enter the height of the pyramid: ");
   int height = numHolder.nextInt();
   //Volume of a square pyramid = 1/3(l * w * h)
   int volume = ((baseLength * baseLength * height) / 3);
   System.out.println("The volume of the pyramid is: " + volume);
   
   
   
 } 
  
  
  
  
}