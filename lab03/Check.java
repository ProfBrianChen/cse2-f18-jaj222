//imports the scanner class
import java.util.Scanner;

//creates the Check class
public class Check{
  
  
  
  
  public static void main(String args[]){
    //creates our scanner to take in user input
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter the original cost of the check in the form xx.xx: ");
    //takes in their input
    double checkCost = myScanner.nextDouble();
    System.out.println("Enter the percentage tip that you wish to pay as a whole number in the form xx: ");
    //takes in their input
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    System.out.println("Enter the number of people who went out to dinner: ");
    //takes in their input
    int numPeople = myScanner.nextInt();
    //creates necessary variables for calculation
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    //calculates the total cost
    totalCost = checkCost* (1 + tipPercent);
    //divides total cost by numPeople to return the cost per person if they all paid equal amounts
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    //get dimes amount and pennies amount using the modulo operation to return remainders
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    //prints results to screen
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

  }
}