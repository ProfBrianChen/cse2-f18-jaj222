  import java.util.Random;
public class Methods {
static String temp1;
static String temp2;
static String temp3;
    public static String adjective(int a){
        String result = " ";
        switch(a){
            case(1):
                result = "quick";
                break;
            
            case(2):
                result = "skilled";
                break;
            case(3):
                result = "great";
                break;
            case(4):
                result = "cold";
                break;
            case(5):
                result = "elegant";
                break;
            case(6):
                result = "ghostly";
                break;
            case(7):
                result = "shiny";
                break;
            case(8):
                result = "colorful";
                break;
            case(9):
                result = "quick";
                break;
            default:
                result = "wild";
                break;
        }
            return result;
    }
    
    public static String subject(int a){
        String result = " ";
        switch(a){
            case(1):
                result = "man";
                break;
            
            case(2):
                result = "fox";
                break;
            case(3):
                result = "coder";
                break;
            case(4):
                result = "woman";
                break;
            case(5):
                result = "leader";
                break;
            case(6):
                result = "song";
                break;
            case(7):
                result = "friend";
                break;
            case(8):
                result = "party";
                break;
            case(9):
                result = "tourist";
                break;
            default:
                result = "skeleton";
                break;
        }
            return result;
    }
    public static String verb(int a){
        String result = " ";
        switch(a){
            case(1):
                result = "hit";
                break;
            
            case(2):
                result = "passed";
                break;
            case(3):
                result = "played";
                break;
            case(4):
                result = "grabbed";
                break;
            case(5):
                result = "walked";
                break;
            case(6):
                result = "tackled";
                break;
            case(7):
                result = "waved";
                break;
            case(8):
                result = "created";
                break;
            case(9):
                result = "envisioned";
                break;
            default:
                result = "portrayed";
                break;
        }
            return result;
    }
    public static String object(int a){
        String result = " ";
        switch(a){
            case(1):
                result = "YEET";
                break;
            
            case(2):
                result = "meme";
                break;
            case(3):
                result = "code";
                break;
            case(4):
                result = "phone";
                break;
            case(5):
                result = "book";
                break;
            case(6):
                result = "program";
                break;
            case(7):
                result = "ball";
                break;
            case(8):
                result = "skull";
                break;
            case(9):
                result = "airplane";
                break;
            default:
                result = "computer";
                break;
        }
            return result;
    }
    //creates 1st sentence that introduces the subject
    public static String sentence1() {
        
        Random randomGenerator = new Random();
        int randomInt1 = randomGenerator.nextInt(10);
        int randomInt2 = randomGenerator.nextInt(10);
        int randomInt3 = randomGenerator.nextInt(10);
        int randomInt4 = randomGenerator.nextInt(10);
        int randomInt5 = randomGenerator.nextInt(10);
        int randomInt6 = randomGenerator.nextInt(10);
        String adj1 = adjective(randomInt1);
        String adj2 = adjective(randomInt2);
        String subj = subject(randomInt3);
        String verb = verb(randomInt4);
        String adj3 = adjective(randomInt5);
        String obj = object(randomInt6);
        temp1 = subj;
        return("The " + adj1 + " " + adj2 + " " +  subj + " " + verb + " " + "the " + adj3 + " " + obj);
    }
  //creates second sentence
    public static String sentence2() {
        // TODO code application logic here
        Random randomGenerator = new Random();
        int randomInt1 = randomGenerator.nextInt(10);
        int randomInt2 = randomGenerator.nextInt(10);
        int randomInt3 = randomGenerator.nextInt(10);
        int randomInt4 = randomGenerator.nextInt(10);
        int randomInt5 = randomGenerator.nextInt(10);
        int randomInt6 = randomGenerator.nextInt(10);
        String adj1 = adjective(randomInt1);
        String adj2 = adjective(randomInt2);
        String subj = subject(randomInt3);
        String verb = verb(randomInt4);
        String adj3 = adjective(randomInt5);
        String obj = object(randomInt6);
        if(subj.equals(temp1)){
            temp2 = "It";
            return(temp2 + " " + verb + " " + "the " + adj3 + " " + obj);
        }else{
            temp2 = subj;
            return("The " + adj1 + " " + adj2 + " " +  temp2 + " " + verb + " " + "the " + adj3 + " " + obj);
        }
        
    } 
  //creates 3rd sentence as a concluding sentence
    public static String sentence3() {
        // TODO code application logic here
        Random randomGenerator = new Random();
        int randomInt1 = randomGenerator.nextInt(10);
        int randomInt2 = randomGenerator.nextInt(10);
        int randomInt3 = randomGenerator.nextInt(10);
        int randomInt4 = randomGenerator.nextInt(10);
        int randomInt5 = randomGenerator.nextInt(10);
        int randomInt6 = randomGenerator.nextInt(10);
        String adj1 = adjective(randomInt1);
        String adj2 = adjective(randomInt2);
        String subj = subject(randomInt3);
        String verb = verb(randomInt4);
        String adj3 = adjective(randomInt5);
        String obj = object(randomInt6);
        if(subj.equals(temp1)){
            temp2 = "It";
            return(temp2 + " " + verb + " " + "the " + adj3 + " " + obj);
        }else{
            temp2 = temp1;
            return("That " + adj1 + " " + adj2 + " " +  temp2 + " " + verb + " " + "the " + adj3 + " " + obj);
        }
        
    }
  //Final Paragraph combining all 3 methods)
    public static String paragraph() {
    return sentence1()  + ". \n" + sentence2()+ ". \n" + sentence3() + ".";
    
    }
    public static void main(String[] args) {
        
       System.out.println(paragraph());
    
}
}

