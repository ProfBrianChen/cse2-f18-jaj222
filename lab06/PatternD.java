import java.util.Scanner;


public class PatternD {
    public static void main(String[] args){
    int r = 0;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please give an integer value for the number of rows.");
    r = scanner.nextInt();
    
    for(int a = r; a >= 1; a--){
        for(int b = a; b>=1; b--){
            System.out.print(b + " ");
        }
        System.out.println();
}
}
}