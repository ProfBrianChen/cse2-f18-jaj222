import java.util.Scanner;

public class PatternA {
    public static void main(String[] args){
    int r = 0;
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please give an integer value for the number of rows.");
    r = scanner.nextInt();
    
    for(int a = 1; a <= r; a++){
        for(int b = 1; b<=a; b++){
            System.out.print(b + " ");
        }
        System.out.println();
}
}
}