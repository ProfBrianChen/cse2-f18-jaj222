/*Jakob Jasin
CSE 002 Fundamentals of Programming
9/6/18
*/
public class Cyclometer{
  //declares the class Cyclometer
  public static void main (String args[]){
    //Main method as per every Java program every Java program ever.
    //input data  and variables below
    
     int secsTrip1=480;  //amount of time in seconds the first trip took
     int secsTrip2=3220;  //amount of time in seconds the second trip took
     int countsTrip1=1561;  //number of rotations in trip 1
     int countsTrip2=9037; //number of rotations in trip 2
     double wheelDiameter=27.0,  //Diameter of the wheel on the bicycle
     PI=3.14159, //useful for calculating circumference of wheel
     feetPerMile=5280,  //used for conversions for calculating trip distance in miles
     inchesPerFoot=12,   //used for conversions of distance
     secondsPerMinute=60;  //used for conversion to claculate time in hours/mins format
     double distanceTrip1, distanceTrip2,totalDistance;  //Used to store the distances calculated
    
    
    System.out.println("Trip 1 took "+
                (secsTrip1/secondsPerMinute)+" minutes and had "+
                 countsTrip1+" counts.");
           System.out.println("Trip 2 took "+
                (secsTrip2/secondsPerMinute)+" minutes and had "+
                 countsTrip2+" counts.");

    distanceTrip1=countsTrip1*wheelDiameter*PI;
        /* Above gives distance in inches
        (for each count, a rotation of the wheel travels
        the diameter in inches times PI)*/
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;

    //Prints out lines with the distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

  }
  
  
}

