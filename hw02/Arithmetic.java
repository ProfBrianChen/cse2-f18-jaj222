/*JJ Jasin
CSE 002
09/10/18
Carr
*/
import java.text.DecimalFormat;
public class Arithmetic{
  
public static void main(String args[]){

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//Cost per belt
double beltPrice = 33.99;

//Pennsylvania's tax rate
double paSalesTax = 0.06;

  //total cost of all pants bought
double totalCostPants = numPants * pantsPrice;
  //total cost of all shirts bought
double totalCostShirts = numShirts * shirtPrice;
  //total cost of all belts bought
double totalCostBelts = numBelts * beltPrice;
  //sales tax charged on the total cost of the pants
double salesTaxPants = totalCostPants * paSalesTax;
  //sales tax charged on the total cost of the shirts
double salesTaxShirts = totalCostShirts * paSalesTax;
  //sales tax charged on the total cost of the belts
double salesTaxBelts = totalCostBelts * paSalesTax;
  //total cost of the purchase before sales tax
double totalCost = totalCostBelts + totalCostShirts + totalCostPants;
  //total amount of sales tax charged from all 3 purchases
double totalTax = salesTaxBelts + salesTaxShirts + salesTaxPants;
  //the total amount paid for the transaction including sales tax
double totalTransaction = totalCost + totalTax;
  DecimalFormat df = new DecimalFormat("###.##");

  System.out.println("Total Cost of Pants: " + df.format(totalCostPants));
  System.out.println("Total Cost of Shirts: " + df.format(totalCostShirts));
  System.out.println("Total Cost of Belts: " + df.format(totalCostBelts));
  System.out.println("Total Tax for Pants: " + df.format(salesTaxPants));
  System.out.println("Total Tax for Shirts: " + df.format(salesTaxShirts));
  System.out.println("Total Tax for Belts: " + df.format(salesTaxBelts));
  System.out.println("Total Cost of Purchase: " + df.format(totalCost));
  System.out.println("Total Tax for Purchase: " + df.format(totalTax));
  System.out.println("Total Cost of Purchase with Tax: " + df.format(totalTransaction));

  
}
}