import java.util.Scanner;
//imports scanner to allow for user input
public class CrapsIf{
  //main method of every program in java
  public static void main(String args[]){
    //Variables required to call for random dice or to be stored with new values.
    int int1 = (int)(Math.random() * 5) + 1;
    int int2 = (int)(Math.random() * 5) + 1;
    String userResponse = " ";
    Scanner input = new Scanner(System.in);
    System.out.println("Would you like to input your own dice combination, or have one randomly generated?  Please write Yes or No");
    userResponse = input.next();
    //prompts user to input their desired values
    if (userResponse.equals("Yes") || userResponse.equals("yes")){
      System.out.println("Please write the first die value from 1-6 inclusive");
      int1 = input.nextInt();
      System.out.println("Please write the second die value from 1-6 inclusive");
      int2 = input.nextInt();
            
    }else{
      System.out.println("What you entered is not valid, please only type yes or no");
        userResponse = input.next();
        if (userResponse.equals("Yes") || userResponse.equals("yes")){
          System.out.println("Please write the first die value from 1-6 inclusive");
          int1 = input.nextInt();
          System.out.println("Please write the second die value from 1-6 inclusive");
          int2 = input.nextInt();
    }
    } 
    
    
    //runs through all dice combos and returns their slang names
    if(int1 == 1 && int2 == 1){
      System.out.println("Snake Eyes");
    }else if(int1 == 1 && int2 == 2 || int1 == 2 && int2 == 1){
      System.out.println("Ace Deuce");
    }else if(int1 == 2 && int2 == 2){
      System.out.println("Hard Four");
    }else if(int1 == 1 && int2 == 3 || int1 == 3 && int2 == 1){
      System.out.println("Easy Four");
    }else if(int1 == 1 && int2 == 4 || int1 == 4 && int2 == 1 || int1 == 2 && int2 == 3 || int1 == 3 && int2 == 2){
      System.out.println("Fever Five");
    }else if(int1 == 1 && int2 == 5 || int1 == 5 && int2 == 1 || int1 == 2 && int2 == 4 || int1 == 4 && int2 == 2){
      System.out.println("Easy Six");
    }else if(int1 == 3 && int2 == 3){
      System.out.println("Hard Six");
    }else if(int1 == 1 && int2 == 6 || int1 == 6 && int2 == 1 || int1 == 2 && int2 == 5 || int1 == 5 && int2 == 2 || int1 == 3 && int2 == 4 || int1 == 4 && int2 == 3){
      System.out.println("Seven Out");
    }else if(int1 == 2 && int2 == 6 || int1 == 6 && int2 == 2 || int1 == 3 && int2 == 5 || int1 == 5 && int2 == 3){
      System.out.println("Easy Eight");
    }else if(int1 == 4 && int2 == 4){
      System.out.println("Hard Eight");
    }else if(int1 == 3 && int2 == 6 || int1 == 6 && int2 == 3 || int1 == 4 && int2 == 5 || int1 == 5 && int2 == 4){
      System.out.println("Nine");
    }else if(int1 == 6 && int2 == 4 || int1 == 4 && int2 == 6){
      System.out.println("Easy Ten");
    }else if(int1 == 5 && int2 == 5 || int1 == 5 && int2 == 5){
      System.out.println("Hard Ten");
    }else if(int1 == 6 && int2 == 5 || int1 == 5 && int2 == 6){
      System.out.println("Yo-leven");
    }else if(int1 == 6 && int2 == 6 || int1 == 6 && int2 == 6){
      System.out.println("Boxcars");
    }else{
      System.out.println("Invalid Roll!  Try picking a number between 1 and 6!");
      //Catch for all invalid values for a 6 sided die.
    }
  }
  
  
  
  
  
}