import java.util.Scanner;
import java.text.DecimalFormat;
public class HandProbability{

public static void main(String[] args){

Scanner scnr = new Scanner(System.in);
int card1 = (int)(Math.random()*52)+1;
int card2 = (int)(Math.random()*52)+1;
int card3 = (int)(Math.random()*52)+1;
int card4 = (int)(Math.random()*52)+1;
int card5 = (int)(Math.random()*52)+1;
int counter = 0;
int oneCounter = 0;
int twoCounter = 0;
int threeCounter = 0;
int fourCounter = 0;
int userHandAmount = 0;
double onePairProb = 0;
double twoPairProb = 0;
double threeKindProb = 0;
double fourKindProb = 0;
boolean logic = false;
while(logic == false){
System.out.println("Please give an integer number of hands you would like to be generated.");
logic = scnr.hasNextInt();
if(logic){
userHandAmount = scnr.nextInt();
}else{
scnr.next();
}
}

while(counter < userHandAmount){
  card1 = (int)(Math.random()*52)+1;
  card2 = (int)(Math.random()*52)+1;
  card3 = (int)(Math.random()*52)+1;
  card4 = (int)(Math.random()*52)+1;
  card5 = (int)(Math.random()*52)+1;
	while (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5){
	card1 = (int)(Math.random()*52)+1;
	}

	while (card2 == card1 || card2 == card3 || card2 == card4 || card2 == card5){
	 card2 = (int)(Math.random()*52)+1;
	}

	while (card3 == card2 || card3 == card1 || card3 == card4 || card3 == card5){
	 card3 = (int)(Math.random()*52)+1;
	}

	while (card4 == card2 || card4 == card3 || card4 == card1 || card4 == card5){
	 card4 = (int)(Math.random()*52)+1;
	}

	while (card5 == card2 || card5 == card3 || card5 == card4 || card5 == card1){
	 card5 = (int)(Math.random()*52)+1;
	}
	if((card1 % 13) == (card2 % 13) || (card1 % 13 == card3 % 13) || (card1 % 13 == card4 % 13) || (card1 % 13 == card5%13) || (card2%13 == card3%13) || (card2%13 == card4%13) || (card2%13 == card5%13) || (card3%13 == card4%13) || (card4%13 == card5%13)){
		oneCounter += 1;
		counter = counter + 1;
	}	
	if((card1 % 13 == card2 % 13) && (card3 % 13 == card4 % 13) || (card1 % 13 == card2 % 13) && (card3 % 13 == card5 % 13) || 
           (card1 % 13 == card4 % 13) && (card2 % 13 == card3 % 13) || 
 	         card1 % 13 == card3 % 13 && card2 % 13 == card4 % 13 || 
           card1 % 13 == card2 % 13 && card3 % 13 == card4 % 13 ||   
           card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13 ||    
           card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13 ||  
           card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13 ||  
	         card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13 ||   
           card1 % 13 == card5 % 13 && card2 % 13 == card4 % 13 ||  
           card1 % 13 == card5 % 13 && card2 % 13 == card3 % 13 ||  
           card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13){   
		twoCounter += 1;
		counter++;
          }
        if((card1%13 == card2 % 13 && card3% 13 == card1%13) || 
           (card1%13 == card2 % 13 && card4% 13 == card1%13) || 
           (card1%13 == card2 % 13 && card5% 13 == card1%13) || 
           (card1%13 == card3 % 13 && card4% 13 == card1%13) || 
           (card1%13 == card3 % 13 && card5% 13 == card1%13) || 
           (card1%13 == card4 % 13 && card5% 13 == card1%13) || 
	         (card2%13 == card3 % 13 && card4% 13 == card2%13) || 
	         (card2%13 == card3 % 13 && card5% 13 == card2%13) || 
	         (card2%13 == card4 % 13 && card5% 13 == card2%13) || 
	         (card3%13 == card4 % 13 && card5% 13 == card3%13)){ 
	   	threeCounter += 1;
		counter++;
 	}
	if(((card1 %13 == card2 % 13) && (card3 % 13  == card1%13) && (card4 % 13 == card1%13)) || 
	   ((card1 %13 == card2 % 13) && (card3 % 13  == card1%13) && (card5 % 13 == card1%13)) || 
	   ((card1 %13 == card2 % 13) && (card4 % 13  == card1%13) && (card5 % 13 == card1%13)) || 
	   ((card1 %13 == card3 % 13) && (card4 % 13  == card1%13) && (card5 % 13 == card1%13)) ||
     ((card2 %13 == card3 % 13) && (card4 % 13  == card2%13) && (card5 % 13 == card2%13))){ 
           	fourCounter += 1;
		counter++;
	}
  counter++;
}
  System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);
  DecimalFormat df = new DecimalFormat(".###");
onePairProb = (double)oneCounter / userHandAmount;
twoPairProb = (double)twoCounter / userHandAmount;
threeKindProb = (double)threeCounter / userHandAmount;
fourKindProb = (double)fourCounter / userHandAmount;
System.out.println("Number of loops: " + userHandAmount);
System.out.println("One Pair Probability: " + df.format(onePairProb));
System.out.println("Two Pair Probability: " + df.format(twoPairProb));
System.out.println("Three-of-a-Kind Probability: " + df.format(threeKindProb));
System.out.println("Four-of-a-Kind Probability: " + fourKindProb);
}
}